<?php

/*
	Plugin Name: WordPress with Composer and Docker
	Plugin URI: https://gitlab.com/nebulon42/wordpress-composer-docker
	Description: Adds warnings for WordPress that is build with Composer and runs on Docker
	Version: 1.0.0
	Author: Michael Glanznig
	Text Domain: wordpress-composer-docker
	Domain Path: /languages
	Author URI: https://gitlab.com/nebulon42
 */

 function wpcd_admin_notice() {
	$plugin_themes_pages = array( 'theme-install', 'theme-install-network', 'themes', 'themes-network', 'plugin-install', 'plugin-install-network', 'plugins', 'plugins-network' );
	$update_pages = array ( 'update-core', 'update-core-network' );
	$admin_page = get_current_screen();
	if( in_array( $admin_page->base, $plugin_themes_pages ) ) {
	?>
		<div class="notice notice-warning">
				<p><?php _e( '<strong>This WordPress installation is built using Composer and runs on Docker! This has an effect on installing themes and plugins.</strong><br>' .
									'<a href="https://en.wikipedia.org/wiki/Composer_(software)">Composer</a> is a package and dependency manager for PHP and ' .
									'<a href="https://en.wikipedia.org/wiki/Docker_(software)">Docker</a> is a lightweight containerized virtualization solution. ' .
									'Because of that this has the following consequences for this installation:<ol>' .
									'<li>Themes and plugins are not meant to be installed through the web frontend as they are managed ' .
									'via Composer. Instead talk to your administrator so that s/he installs the themes and plugins you need for you. You still ' .
									'can install them here, but once the installation is re-bundled those will vanish.</li>' .
									'<li>Since themes and plugins are managed via Composer any themes and plugins installed or uploaded via the web frontend ' .
									'will vanish whenever the container updates. This is  because Docker containers have a filesystem where at runtime new things ' .
									'can be added (like themes and plugins). But containers are ephemeral. Whenever the container gets teared down and recreated ' .
									'(like in case of an update) those changes are lost forever when they are not saved elsewhere.</li>' .
									'<li>While you can remove themes and plugins via the web frontend those will re-appear when they are part of the Composer ' .
									'configuration. So better tell your administrator that you want to have some themes and plugins removed.</li>' .
									'</ol><strong>You have been warned.</strong>', 'wordpress-composer-docker' ); ?></p>
		</div>
		<?php
	}
	if ( in_array( $admin_page->base, $update_pages ) ) {
	?>
		<div class="notice notice-warning">
				<p><?php _e( '<strong>This WordPress installation is built using Composer and runs on Docker! This has an effect on updating.</strong><br>' .
									'<a href="https://en.wikipedia.org/wiki/Composer_(software)">Composer</a> is a package and dependency manager for PHP and ' .
									'<a href="https://en.wikipedia.org/wiki/Docker_(software)">Docker</a> is a lightweight containerized virtualization solution. ' .
									'Because of that this has the following consequences for this installation:<ol>' .
									'<li>Updates for WordPress Core, themes and plugins come in at build time via Composer and are automatically installed. ' .
									'The only exception are translation updates which have to be still installed by pressing the button here.</li>' .
									'<li>While you still can install updates here via the web frontend when they are not reflected in the build they will ' .
									'disappear the next time the Docker container gets updated. This is because Docker containers have a filesystem where at ' .
									'runtime new things can be added (like themes and plugins). But containers are ephemeral. Whenever the container gets ' .
									'teared down and recreated (like in case of an update) those changes are lost forever when they are not saved elsewhere.</li>' .
									'<li>There should be a periodical re-build so that the WordPress core and its plugins don\'t get outdated. If you notice that ' .
									'plugins are never updated you should talk to your administrator so that s/he sets up a periodical re-build for you.</li>' .
									'</ol><strong>You have been warned.</strong>', 'wordpress-composer-docker' ); ?></p>
		</div>
		<?php
	}
 }
 add_action( 'all_admin_notices', 'wpcd_admin_notice' );

function wpcd_load_plugin_textdomain() {
	load_plugin_textdomain( 'wordpress-composer-docker', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'wpcd_load_plugin_textdomain' );
